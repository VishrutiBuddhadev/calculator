let string = "";
let buttons = document.querySelectorAll('.button');
let resultInput = document.querySelector('input');

Array.from(buttons).forEach((button) => {
    button.addEventListener('click', (e) => {
        const buttonText = e.target.innerHTML;

        if (buttonText === '=') {
            // Evaluate the expression and update the input field
            string = eval(string);
            resultInput.value = string;
        } else if (buttonText === 'C') {
            // Clear the input field
            string = "";
            resultInput.value = string;
        } else if (buttonText === '←') {
            // Handle the backspace (arrow) button
            string = string.slice(0, -1);
            resultInput.value = string;
        } else {
            // Append the button's text to the input field
            string = string + buttonText;
            resultInput.value = string;
        }
    });
});
